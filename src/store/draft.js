export default function(store) {
  return {
    ...store,
    state() {
      const init = store.state();
      return {
        // Warning, because we do only a spread, the clone is only one level deep.
        ...init,
        __saved: { ...init }
      }
    },
    getters: {
      isModified(state) {
        // Removing __saved property from the state
        const { __saved, ...draft } = state;
        return !Object.keys(draft).every(key => __saved[key] === draft[key]);
      },
      ...store.getters
    },
    actions: {
      revert({ commit }) {
        commit('revert');
      },
      save({ commit }) {
        commit('save');
      },
      ...store.actions
    },
    mutations: {
      revert(state) {
        Object.assign(state, state.__saved)
      },
      save(state) {
        // Removing __saved property from the state
        const { __saved, ...draft } = state;
        Object.assign(state.__saved, { ...draft })
      },
      ...store.mutations
    }
  }
}
