import Vue from 'vue'
import Vuex from 'vuex'

import Note from './note';
import Draft from './draft';

Vue.use(Vuex)

const generateId = () => Math.random().toString(36).substring(7);

const store = new Vuex.Store({
  state: {
    notes: []
  },
  actions: {
    addNote({ commit }) {
      // Generate hash;
      const id = generateId();
      commit('addNote', { id });
    },

    deleteNote({ commit }, namespace) {
      commit('deleteNote', { namespace });
    },

    fetchNotes({ commit }) {
      // sample data as we should receive them from API
      const data = [{
        id: generateId(),
        name: "test1",
        message: "testing in 1"
      }, {
        id: generateId(),
        name: "test2",
        message: "testing in 2"
      }]

      data.forEach((item) => commit('addNote', item))
    }
  },
  mutations: {
    addNote(state, { id, ...data }) {
      const namespace = 'notes/' + id;
      const noteStore = Note(data);
      const finalStore = Draft(noteStore);

      store.registerModule(namespace, finalStore);
      state.notes.push(namespace)
    },

    deleteNote(state, { namespace }) {
      const idx = state.notes.indexOf(namespace);
      // simple data protection
      if (idx === -1) return;

      store.unregisterModule(namespace);
      state.notes.splice(idx, 1);
    }
  }
})

export default store;
