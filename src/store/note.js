export default ({ name = "name", message = "Hello World" } = {}) => ({
  namespaced: true,
  state () {
    return {
      name,
      message
    }
  },
  actions: {
    update({ commit },note) {
      commit('update', note);
    }
  },
  mutations: {
    update(state, note) {
      Object.assign(state, { ...note })
    }
  }
})
