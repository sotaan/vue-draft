import { get } from 'lodash';

// state are simply a nested Object
// state.parent1.parent2.myModule === myModule state Object
const generateStateModulePath = (namespace, name) => namespace+"."+name
// getters, mutations and actions are nested using a `/`
// parent1/parent2/myModule/my{Getter,Action,Mutation}
const generateGetterModulePath = (namespace, name) => namespace+"/"+name

const getAppropiateState = (state, ...access) => get(state, generateStateModulePath(...access))

const getAppropiateGetter = (getters, ...access) => getters[generateGetterModulePath(...access)]


// The syncState function takes a list of name.
// And returns an object to insert into the computed properties of a vue.
// Each computed property has a getter that maps directly to the corresponding property in the vuex module
// And a setter that call the update action of the vue, to update the vuex module.
// Obviously, the vue shall expose such an update action, otherwise the update will fail.
export function syncState(names) {
  return names.reduce((synced, name) => {
    return {
      ...synced,
      [name]: {
        get() {
          
          // The way this access the module is not idiomatic
          const state = this.$store.state;
          // What if this namespace is already nested ?
          // lodash can handle this for us if we provide a path
          // like that: 'parent1.parent2.parent3'
          return getAppropiateState(state, this.namespace, name);
        },
        set(payload) {
          // Warning, there must be an update method defined
          return this.update({ [name]: payload })
        }
      }
    }
  }, {})
}

// Similarly to the syncState function, the syncActions function takes a list of names
// And returns an object to insert into the methods of a vue to map vuex module actions to methods.
export function syncActions(names) {
  return names.reduce((synced, name) => {
    return {
      ...synced,
      [name](payload) {
        return this.$store.dispatch(this.namespace + '/' + name, payload);
      }
    }
  }, {})
}



// Similarly, the syncGetters function takes a list of names
// And returns an object to insert into the computed properties of a vue to map vuex getters to computed properties.
export function syncGetters(names) {
  return names.reduce(
    (acc, name) => ({
      ...acc,
      [name]() {
        const getters = this.$store.getters;

        return getAppropiateGetter(getters, this.namespace, name);
      }
    }), {})
}
