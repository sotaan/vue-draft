# vue-draft

> A simple example of a dynamic vuex module

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

---

This repository is an experiment for two concept we are developing around the composition of `vuex` stores.
The two concepts are dynamic composition, and higher-order composition.

This experiment is carried an a simple application of note taking.
The user can add and remove notes to a list.
The notes are composed of a name and message.
Dynamic composition is used for management of the list of notes.
And the modification of each note exhibit a draft mechanism, allowing to discard the current changes applied to revert to a previously saved state.
The higher-order composition is used to extend the behavior of a note, with this draft mechanism.

# Composition

It is quite a personal opinion, but I believe that composition is one of the very few core concept of software engineering.
For the purpose of this explanation, I identify three main composition mechanisms (countless other exists, but shall remain are out of scope in this context).
- nesting
- dynamic
- higher-order

To support this explanation, I will compare these mechanisms between programming language, and `vuex`.
This analogy is quite thin, and is only meant as an illustration. Don't see any grand truth behind it.

In a programming language, the nesting composition is what allows to assemble several instructions into one, to modularize and reduce the immediate complexity.
Immediate exemples are macros, procedures, and functions.
<!-- But the latter introduce other composition mechanisms we shall exclude for this explanation. -->

In vuex, the nesting composition is the ability to nest modules one into the other.
It is well documented [here](https://vuex.vuejs.org/en/modules.html).
The analogy with macros and procedure is somehow immedaite.
`vuex` modules provide encapsulation, similarly to the scope of procedures and functions.

## dynamic composition

The dynamic composition allows to assemble instructions in a way unknown statically, at compile time, and to decide of this assemblage dynamicall, at runtime.
When one uses a `for` loop or a condition, the parameter conditions the execution of certain instructions.

Similarly, in `vuex`, one can dynamicall add modules.
(see _Dynamic Module Registration_ in the (`vuex` module documentation)[https://vuex.vuejs.org/en/modules.html])

As each note is added dynamicaly, so it makes sense to also add the associated module dynamicaly.
But what is really important, is that this composition allows to isolate the state and actions of the list of notes, from the ones of the note themselves.
In the `store.js` file, we have the list of notes, and the actions to add and delete a note.
Whereas in the `note.js` file, we have the state of a note, along with an `update` action.
The main `store.js` doesn't know anything of the state nor actions of a single note. We could completly change the implementation of the note, without impacting the way they are stored.
We have successfully separated the concerns.

### Drawbacks and limitation

In order to reduce the complexity of , I struggled a lot to provide the correct namespaced module to the according `Note.vue` vue.
I isolated all this logic into a `storeHelper.js` file which provide several methods to bind the vue state with the module whose namespace is passed as a props.
But because dynamicall passing a module to a vue is not very straightforward, the process was quite difficult.
And I had to resort to non idomatic operations.

## higher-order composition

What I call here the higher-order composition, is similar (read identical, if you will) to the concept of higher-order function.
It is the ability for a concept to manipulate concept of same class.
A higher-order function accept another function in parameter, and can return a function.
It allows a great range of pattern in functional programming.

In my opinion, `vuex` lacks a similar concept, and therefore, I experimented with the concept of a higher-order module.

In the `draft.js` file, you will find an higher-order `vuex` module.
That is a function that takes a module as parameter, and returns an extended module back.

I used this mechanism to extend the `note.js` module, that exhibit only the `update` action, with two other actions `revert` and `save`, along with the state to support these actions.
The code is quite straight and self-explanatory.
